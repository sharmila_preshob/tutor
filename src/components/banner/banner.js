import React from 'react';
import './banner.scss';

const Banner = () => {
  return (
    <div className="banner">
     <div className="banner-content">
      <h1>The place where Tutors and Students meet</h1>
      <h5>Find out who can help you take your education to next level</h5>
      <button className="banner-btn">Get started now</button>
      </div>
    </div>
  )
}

export default Banner;

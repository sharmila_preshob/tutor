import React from 'react';
import './header.scss';

const Header = () => {
  return (
    <div className="header">
        <div className='logo'>TutorMatch</div>
        <div className='nav'>
          <a className='nav-link' href="#">Become a Tutor</a>
          <button className="btn">Sign In</button>
        </div>
    </div>
  )
}

export default Header;

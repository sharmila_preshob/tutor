import React from 'react';
import './cards.scss';

const Card = (props) => {
  console.log(props.filterCity);
  if (props.filterCity) {
    var data = props.cards.filter(function(card){
       return card.city === props.filterCity;
    });
  }
  else {
    var data = props.cards;
  }

 const result = data.map(card => {
   return (
     <div className="card-item" key={card.id}>
     <img className="card-image" src = {process.env.PUBLIC_URL + './assets/images/user-photo.svg'} alt ="user"/>
     <div className="card-desc">
       <div className="card-name">{card.name}</div>
       <div className="card-city-label">City</div>
       <div className="card-city">{card.city}</div>
       <div className="students">
         <div>Students so far:</div>
         <ul className="student-list">
           <li><img src={process.env.PUBLIC_URL + './assets/images/student-photo.svg'}/></li>
           <li><img src={process.env.PUBLIC_URL + './assets/images/student-photo.svg'}/></li>
           <li><img src={process.env.PUBLIC_URL + './assets/images/student-photo.svg'}/></li>
         </ul>
       </div>
    </div>
    </div>

   )
   })
  return (
    <div className="card">{result}</div>
  )
}

export default Card;

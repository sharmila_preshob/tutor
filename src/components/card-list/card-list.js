import React from 'react';
import Card from '../cards/cards';
import './card-list.scss';

class CardList extends React.Component {
  state= {
    loading: true,
    cards: [],
    filterCity: ''
  }

  componentDidMount () {
    fetch ('../data/data.json')
    .then (res => res.json())
    .then (data => {
      console.log(data);
      this.setState({cards: data})
    })
    .catch(error => console.log('Error:', error));
  }

  sortByName = () => {
    const sortedName = this.state.cards
    .sort(function(a, b) {
     var nameA = a.name.toUpperCase();
     var nameB = b.name.toUpperCase();
     return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
    });
    this.setState({filterCity: '', cards: sortedName});
  }

  sortByCity = () => {
    const sortedCity = this.state.cards
    .sort(function(a, b) {
     var cityA = a.city.toUpperCase();
     var cityB = b.city.toUpperCase();
     return (cityA < cityB) ? -1 : (cityA > cityB) ? 1 : 0;
    });
    this.setState({filterCity: '', cards: sortedCity});
  }

  render () {
    return(
        <div className="main">
          <div className="content">
              <h4>Our Tutors</h4>
              <p>We have more than 100 tutors disciplines ready to match you</p>
          </div>
        <div className="select-row">
          <div className="select-filter">
            <span className="select-label">Filter by: </span>
            <button className="select-btn" onClick={() => this.setState({filterCity: "Liverpool"})}>Liverpool</button>
            <button className="select-btn" onClick={() => this.setState({filterCity: "London"})}>London</button>
            <button className="select-btn" onClick={() => this.setState({filterCity: "Manchester"})}>Manchester</button>
          </div>
          <div className="select-sort">
          <span className="select-label">Sort by: </span>
           <button className="select-btn" onClick={this.sortByName}>Name</button>
           <button className="select-btn" onClick={this.sortByCity}>City</button>
          </div>
        </div>
          <Card cards={this.state.cards} filterCity={this.state.filterCity}/>
          <button className="view-all-btn" onClick={() => this.setState({filterCity: ""})}>See all Tutors</button>
        </div>
    )
  }
}

export default CardList;

import React from 'react';
import Header from './components/header/header';
import Banner from './components/banner/banner';
import CardList from './components/card-list/card-list';
import './App.scss';

function App() {
  return (
    <div className="">
        <Header/>
        <Banner/>
        <CardList/>
    </div>
  );
}

export default App;
